var RouteMaster = (function() {
	"use strict";

	var markdown = null;

	// Constructor
	function RouteMaster() {
		$("#btnWrite").click(this.writeMarkdown);
		$('#btnRead').on("click", this.readHtml);
		$("input[name=mainGroup]:radio").click(this.radioControlsHandler);
	}

	RouteMaster.prototype.writeMarkdown = function(event) {
		$.get('/writeMarkdownToMongo', function(data) {
			alert("success");
		});

	};

	RouteMaster.prototype.readHtml = function(event) {
		$.get('/read', function(data) {
			markdown = data;
			$('#divDisplay').html(data);
		});

	};

	RouteMaster.prototype.radioControlsHandler = function(event) {
		if ($("#radioSection1").is(':checked')) {
			$('#divDisplay').load('read');
		}
		
		if ($("#radioSection1").is(':checked')) {
			$('#divDisplay').load('read h1');
		}

		if ($("#radioSection2").is(':checked')) {
			$('#divDisplay').load('read h2');
		}

		if ($("#radioSection3").is(':checked')) {
			$('#divDisplay').load('read p');
		}

		if ($("#radioSection4").is(':checked')) {
			$('#divDisplay').load('read ol');
		}
	};

	// Return constructor
	return RouteMaster;
}());

$(document).ready(function() {
	"use strict";
	new RouteMaster();
});