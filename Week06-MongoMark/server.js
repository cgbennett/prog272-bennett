var express = require('express');
var app = express();
var fs = require('fs');
var mongo = require('mongodb');
// var serverconfig = require('./serverconfig');
var MongoClient = require('mongodb').MongoClient;
var BSON = mongo.BSONPure;

var fs = require('fs');

var Converter = require("./Markdown.Converter").Converter;
var getSanitizingConverter = require("./Markdown.Sanitizer").getSanitizingConverter;
var conv = new Converter();
var saneConv = getSanitizingConverter();

app.use(express.bodyParser());

var port = process.env.PORT || 30025;
app.use("/", express.static(__dirname + '/Public'));
app.listen(port);
console.log('Listening on port :' + port);

var mongodb_url = "mongodb://chris:hola@dbh73.mongolab.com:27737/markdowndb";

// Served up as the default page when a request comes from the client.
app.get('/', function(request, result) {
	var html = fs.readFileSync(__dirname + '/Public/index.html');
	result.writeHeader(200, {
		"Content-Type" : "text/html"
	});
	result.write(html);
	result.end();
});

app.get('/writeMarkdownToMongo', function(req, res) {
	console.log('/writeMarkdownToMongo called');
	var markdown = fs.readFileSync('example.md', 'utf8');

	MongoClient.connect(mongodb_url, function(err, database) {
		if (err) {
			throw err;
		}

		var collection = database.collection('markdown');
		collection.insert({
			"filename" : "example.md",
			"markdown" : markdown
		}, function(err, data) {
			if (err) {
				console.log(err);
				return;
			}
			console.log("success");
			res.send({message:'Success'});
		});

	});
});

app.get('/read', function(request, response) {
	"use strict";
	console.log('/read called');
	MongoClient.connect(mongodb_url, function(err, database) {
		if (err) {
			throw err;
		}
		var collection = database.collection('markdown');
		collection.find().toArray(function(err, theArray) {
			console.log(theArray[0].markdown);
			// Convert Markdown to HTML
			var html = conv.makeHtml(theArray[0].markdown);
			console.log(html);
			database.close();
			response.send(html);
		});
	});
});
