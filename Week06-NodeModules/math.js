
var mathObj = {
	add : function(operand1, operand2) {
		return operand1 + operand2;
	},
	multiply : function(operand1, operand2) {
		return operand1 * operand2;
	},
	subtract : function(operand1, operand2) {
		return operand1 - operand2;
	}
};

module.exports = mathObj;
