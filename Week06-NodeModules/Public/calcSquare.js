
var SquareObj = (function() {
	'use strict';

	// Globals

	function Square() {
	}

	Square.prototype.calcSquare = function(operand) {
		return operand * operand;
	};

	return Square;

})();

module.exports = new SquareObj();
