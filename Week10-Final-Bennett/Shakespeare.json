[
{
   "title":"Sonnet 1",
   "content": "From fairest creatures we desire increase,\nThat thereby beauty's rose might never die,\nBut as the riper should by time decease,\nHis tender heir might bear his memory:\nBut thou contracted to thine own bright eyes,\nFeed'st thy light's flame with self-substantial fuel,\nMaking a famine where abundance lies,\nThy self thy foe, to thy sweet self too cruel:\nThou that art now the world's fresh ornament,\nAnd only herald to the gaudy spring,\nWithin thine own bud buriest thy content,\nAnd tender churl mak'st waste in niggarding:\nPity the world, or else this glutton be,\nTo eat the world's due, by the grave and thee."
},
{
   "title":"Sonnet 2",
   "content": "When forty winters shall besiege thy brow,\nAnd dig deep trenches in thy beauty's field,\nThy youth's proud livery so gazed on now,\nWill be a tatter'd weed of small worth held:\nThen being asked, where all thy beauty lies,\nWhere all the treasure of thy lusty days;\nTo say, within thine own deep sunken eyes,\nWere an all-eating shame, and thriftless praise.\nHow much more praise deserv'd thy beauty's use,\nIf thou couldst answer 'This fair child of mine\nShall sum my count, and make my old excuse,'\nProving his beauty by succession thine!\nThis were to be new made when thou art old,\nAnd see thy blood warm when thou feel'st it cold."
},
{
   "title":"Sonnet 3",
   "content": "Look in thy glass and tell the face thou viewest\nNow is the time that face should form another;\nWhose fresh repair if now thou not renewest,\nThou dost beguile the world, unbless some mother.\nFor where is she so fair whose unear'd womb\nDisdains the tillage of thy husbandry?\nOr who is he so fond will be the tomb,\nOf his self-love to stop posterity?\nThou art thy mother's glass and she in thee\nCalls back the lovely April of her prime;\nSo thou through windows of thine age shalt see,\nDespite of wrinkles this thy golden time.\nBut if thou live, remember'd not to be,\nDie single and thine image dies with thee."
},
{
   "title":"Sonnet 4",
   "content": "Unthrifty loveliness, why dost thou spend\nUpon thy self thy beauty's legacy?\nNature's bequest gives nothing, but doth lend,\nAnd being frank she lends to those are free:\nThen, beauteous niggard, why dost thou abuse\nThe bounteous largess given thee to give?\nProfitless usurer, why dost thou use\nSo great a sum of sums, yet canst not live?\nFor having traffic with thy self alone,\nThou of thy self thy sweet self dost deceive:\nThen how when nature calls thee to be gone,\nWhat acceptable audit canst thou leave?\nThy unused beauty must be tombed with thee,\nWhich, used, lives th' executor to be."
},
{
   "title":"Sonnet 5",
   "content": "Those hours, that with gentle work did frame\nThe lovely gaze where every eye doth dwell,\nWill play the tyrants to the very same\nAnd that unfair which fairly doth excel;\nFor never-resting time leads summer on\nTo hideous winter, and confounds him there;\nSap checked with frost, and lusty leaves quite gone,\nBeauty o'er-snowed and bareness every where:\nThen were not summer's distillation left,\nA liquid prisoner pent in walls of glass,\nBeauty's effect with beauty were bereft,\nNor it, nor no remembrance what it was:\nBut flowers distill'd, though they with winter meet,\nLeese but their show; their substance still lives sweet."
},
{
   "title":"Sonnet 6",
   "content": "Then let not winter's ragged hand deface,\nIn thee thy summer, ere thou be distill'd:\nMake sweet some vial; treasure thou some place\nWith beauty's treasure ere it be self-kill'd.\nThat use is not forbidden usury,\nWhich happies those that pay the willing loan;\nThat's for thy self to breed another thee,\nOr ten times happier, be it ten for one;\nTen times thy self were happier than thou art,\nIf ten of thine ten times refigur'd thee:\nThen what could death do if thou shouldst depart,\nLeaving thee living in posterity?\nBe not self-will'd, for thou art much too fair\nTo be death's conquest and make worms thine heir."
},
{
   "title":"Sonnet 7",
   "content": "Lo! in the orient when the gracious light\nLifts up his burning head, each under eye\nDoth homage to his new-appearing sight,\nServing with looks his sacred majesty;\nAnd having climb'd the steep-up heavenly hill,\nResembling strong youth in his middle age,\nYet mortal looks adore his beauty still,\nAttending on his golden pilgrimage:\nBut when from highmost pitch, with weary car,\nLike feeble age, he reeleth from the day,\nThe eyes, 'fore duteous, now converted are\nFrom his low tract, and look another way:\nSo thou, thyself outgoing in thy noon:\nUnlook'd, on diest unless thou get a son."
},
{
   "title":"Sonnet 8",
   "content": "Music to hear, why hear'st thou music sadly?\nSweets with sweets war not, joy delights in joy:\nWhy lov'st thou that which thou receiv'st not gladly,\nOr else receiv'st with pleasure thine annoy?\nIf the true concord of well-tuned sounds,\nBy unions married, do offend thine ear,\nThey do but sweetly chide thee, who confounds\nIn singleness the parts that thou shouldst bear.\nMark how one string, sweet husband to another,\nStrikes each in each by mutual ordering;\nResembling sire and child and happy mother,\nWho, all in one, one pleasing note do sing:\nWhose speechless song being many, seeming one,\nSings this to thee: 'Thou single wilt prove none.'"
},
{
   "title":"Sonnet 9",
   "content": "Is it for fear to wet a widow's eye,\nThat thou consum'st thy self in single life?\nAh! if thou issueless shalt hap to die,\nThe world will wail thee like a makeless wife;\nThe world will be thy widow and still weep\nThat thou no form of thee hast left behind,\nWhen every private widow well may keep\nBy children's eyes, her husband's shape in mind:\nLook! what an unthrift in the world doth spend\nShifts but his place, for still the world enjoys it;\nBut beauty's waste hath in the world an end,\nAnd kept unused the user so destroys it.\nNo love toward others in that bosom sits\nThat on himself such murd'rous shame commits."
},
{
   "title":"Sonnet 10",
   "content": "For shame! deny that thou bear'st love to any,\nWho for thy self art so unprovident.\nGrant, if thou wilt, thou art belov'd of many,\nBut that thou none lov'st is most evident:\nFor thou art so possess'd with murderous hate,\nThat 'gainst thy self thou stick'st not to conspire,\nSeeking that beauteous roof to ruinate\nWhich to repair should be thy chief desire.\nO! change thy thought, that I may change my mind:\nShall hate be fairer lodg'd than gentle love?\nBe, as thy presence is, gracious and kind,\nOr to thyself at least kind-hearted prove:\nMake thee another self for love of me,\nThat beauty still may live in thine or thee."
},
{
   "title":"Sonnet 11",
   "content": "As fast as thou shalt wane, so fast thou grow'st,\nIn one of thine, from that which thou departest;\nAnd that fresh blood which youngly thou bestow'st,\nThou mayst call thine when thou from youth convertest,\nHerein lives wisdom, beauty, and increase;\nWithout this folly, age, and cold decay:\nIf all were minded so, the times should cease\nAnd threescore year would make the world away.\nLet those whom nature hath not made for store,\nHarsh, featureless, and rude, barrenly perish:\nLook, whom she best endow'd, she gave thee more;\nWhich bounteous gift thou shouldst in bounty cherish:\nShe carv'd thee for her seal, and meant thereby,\nThou shouldst print more, not let that copy die."
},
{
   "title":"Sonnet 12",
   "content": "When I do count the clock that tells the time,\nAnd see the brave day sunk in hideous night;\nWhen I behold the violet past prime,\nAnd sable curls, all silvered o'er with white;\nWhen lofty trees I see barren of leaves,\nWhich erst from heat did canopy the herd,\nAnd summer's green all girded up in sheaves,\nBorne on the bier with white and bristly beard,\nThen of thy beauty do I question make,\nThat thou among the wastes of time must go,\nSince sweets and beauties do themselves forsake\nAnd die as fast as they see others grow;\nAnd nothing 'gainst Time's scythe can make defence\nSave breed, to brave him when he takes thee hence."
},
{
   "title":"Sonnet 13",
   "content": "O! that you were your self; but, love you are\nNo longer yours, than you your self here live:\nAgainst this coming end you should prepare,\nAnd your sweet semblance to some other give:\nSo should that beauty which you hold in lease\nFind no determination; then you were\nYourself again, after yourself's decease,\nWhen your sweet issue your sweet form should bear.\nWho lets so fair a house fall to decay,\nWhich husbandry in honour might uphold,\nAgainst the stormy gusts of winter's day\nAnd barren rage of death's eternal cold?\nO! none but unthrifts. Dear my love, you know,\nYou had a father: let your son say so."
},
{
   "title":"Sonnet 14",
   "content": "Not from the stars do I my judgement pluck;\nAnd yet methinks I have astronomy,\nBut not to tell of good or evil luck,\nOf plagues, of dearths, or seasons' quality;\nNor can I fortune to brief minutes tell,\nPointing to each his thunder, rain and wind,\nOr say with princes if it shall go well\nBy oft predict that I in heaven find:\nBut from thine eyes my knowledge I derive,\nAnd constant stars in them I read such art\nAs 'Truth and beauty shall together thrive,\nIf from thyself, to store thou wouldst convert';\nOr else of thee this I prognosticate:\n'Thy end is truth's and beauty's doom and date.'"
},
{
   "title":"Sonnet 15",
   "content": "When I consider every thing that grows\nHolds in perfection but a little moment,\nThat this huge stage presenteth nought but shows\nWhereon the stars in secret influence comment;\nWhen I perceive that men as plants increase,\nCheered and checked even by the self-same sky,\nVaunt in their youthful sap, at height decrease,\nAnd wear their brave state out of memory;\nThen the conceit of this inconstant stay\nSets you most rich in youth before my sight,\nWhere wasteful Time debateth with decay\nTo change your day of youth to sullied night,\nAnd all in war with Time for love of you,\nAs he takes from you, I engraft you new."
},
{
   "title":"Sonnet 16",
   "content": "But wherefore do not you a mightier way\nMake war upon this bloody tyrant, Time?\nAnd fortify your self in your decay\nWith means more blessed than my barren rhyme?\nNow stand you on the top of happy hours,\nAnd many maiden gardens, yet unset,\nWith virtuous wish would bear you living flowers,\nMuch liker than your painted counterfeit:\nSo should the lines of life that life repair,\nWhich this, Time's pencil, or my pupil pen,\nNeither in inward worth nor outward fair,\nCan make you live your self in eyes of men.\nTo give away yourself, keeps yourself still,\nAnd you must live, drawn by your own sweet skill."
},
{
   "title":"Sonnet 17",
   "content": "Who will believe my verse in time to come,\nIf it were fill'd with your most high deserts?\nThough yet heaven knows it is but as a tomb\nWhich hides your life, and shows not half your parts.\nIf I could write the beauty of your eyes,\nAnd in fresh numbers number all your graces,\nThe age to come would say 'This poet lies;\nSuch heavenly touches ne'er touch'd earthly faces.'\nSo should my papers, yellow'd with their age,\nBe scorn'd, like old men of less truth than tongue,\nAnd your true rights be term'd a poet's rage\nAnd stretched metre of an antique song:\nBut were some child of yours alive that time,\nYou should live twice,—in it, and in my rhyme."
},
{
   "title":"Sonnet 18",
   "content": "Shall I compare thee to a summer's day?\nThou art more lovely and more temperate:\nRough winds do shake the darling buds of May,\nAnd summer's lease hath all too short a date:\nSometime too hot the eye of heaven shines,\nAnd often is his gold complexion dimm'd,\nAnd every fair from fair sometime declines,\nBy chance, or nature's changing course untrimm'd:\nBut thy eternal summer shall not fade,\nNor lose possession of that fair thou ow'st,\nNor shall death brag thou wander'st in his shade,\nWhen in eternal lines to time thou grow'st,\nSo long as men can breathe, or eyes can see,\nSo long lives this, and this gives life to thee."
},
{
   "title":"Sonnet 19",
   "content": "Devouring Time, blunt thou the lion's paws,\nAnd make the earth devour her own sweet brood;\nPluck the keen teeth from the fierce tiger's jaws,\nAnd burn the long-liv'd phoenix, in her blood;\nMake glad and sorry seasons as thou fleets,\nAnd do whate'er thou wilt, swift-footed Time,\nTo the wide world and all her fading sweets;\nBut I forbid thee one most heinous crime:\nO! carve not with thy hours my love's fair brow,\nNor draw no lines there with thine antique pen;\nHim in thy course untainted do allow\nFor beauty's pattern to succeeding men.\nYet, do thy worst old Time: despite thy wrong,\nMy love shall in my verse ever live young."
},
{
   "title":"Sonnet 20",
   "content": "A woman's face with nature's own hand painted,\nHast thou, the master mistress of my passion;\nA woman's gentle heart, but not acquainted\nWith shifting change, as is false women's fashion:\nAn eye more bright than theirs, less false in rolling,\nGilding the object whereupon it gazeth;\nA man in hue all 'hues' in his controlling,\nWhich steals men's eyes and women's souls amazeth.\nAnd for a woman wert thou first created;\nTill Nature, as she wrought thee, fell a-doting,\nAnd by addition me of thee defeated,\nBy adding one thing to my purpose nothing.\nBut since she prick'd thee out for women's pleasure,\nMine be thy love and thy love's use their treasure."
},
{
   "title":"Sonnet 21",
   "content": "So is it not with me as with that Muse,\nStirr'd by a painted beauty to his verse,\nWho heaven itself for ornament doth use\nAnd every fair with his fair doth rehearse,\nMaking a couplement of proud compare'\nWith sun and moon, with earth and sea's rich gems,\nWith April's first-born flowers, and all things rare,\nThat heaven's air in this huge rondure hems.\nO! let me, true in love, but truly write,\nAnd then believe me, my love is as fair\nAs any mother's child, though not so bright\nAs those gold candles fix'd in heaven's air:\nLet them say more that like of hearsay well;\nI will not praise that purpose not to sell."
},
{
   "title":"Sonnet 22",
   "content": "My glass shall not persuade me I am old,\nSo long as youth and thou are of one date;\nBut when in thee time's furrows I behold,\nThen look I death my days should expiate.\nFor all that beauty that doth cover thee,\nIs but the seemly raiment of my heart,\nWhich in thy breast doth live, as thine in me:\nHow can I then be elder than thou art?\nO! therefore love, be of thyself so wary\nAs I, not for myself, but for thee will;\nBearing thy heart, which I will keep so chary\nAs tender nurse her babe from faring ill.\nPresume not on thy heart when mine is slain,\nThou gav'st me thine not to give back again."
},
{
   "title":"Sonnet 23",
   "content": "As an unperfect actor on the stage,\nWho with his fear is put beside his part,\nOr some fierce thing replete with too much rage,\nWhose strength's abundance weakens his own heart;\nSo I, for fear of trust, forget to say\nThe perfect ceremony of love's rite,\nAnd in mine own love's strength seem to decay,\nO'ercharg'd with burthen of mine own love's might.\nO! let my looks be then the eloquence\nAnd dumb presagers of my speaking breast,\nWho plead for love, and look for recompense,\nMore than that tongue that more hath more express'd.\nO! learn to read what silent love hath writ:\nTo hear with eyes belongs to love's fine wit."
},
{
   "title":"Sonnet 24",
   "content": "Mine eye hath play'd the painter and hath stell'd,\nThy beauty's form in table of my heart;\nMy body is the frame wherein 'tis held,\nAnd perspective it is best painter's art.\nFor through the painter must you see his skill,\nTo find where your true image pictur'd lies,\nWhich in my bosom's shop is hanging still,\nThat hath his windows glazed with thine eyes.\nNow see what good turns eyes for eyes have done:\nMine eyes have drawn thy shape, and thine for me\nAre windows to my breast, where-through the sun\nDelights to peep, to gaze therein on thee;\nYet eyes this cunning want to grace their art,\nThey draw but what they see, know not the heart."
},
{
   "title":"Sonnet 25",
   "content": "Let those who are in favour with their stars\nOf public honour and proud titles boast,\nWhilst I, whom fortune of such triumph bars\nUnlook'd for joy in that I honour most.\nGreat princes' favourites their fair leaves spread\nBut as the marigold at the sun's eye,\nAnd in themselves their pride lies buried,\nFor at a frown they in their glory die.\nThe painful warrior famoused for fight,\nAfter a thousand victories once foil'd,\nIs from the book of honour razed quite,\nAnd all the rest forgot for which he toil'd:\nThen happy I, that love and am belov'd,\nWhere I may not remove nor be remov'd."
},
{
   "title":"Sonnet 26",
   "content": "Lord of my love, to whom in vassalage\nThy merit hath my duty strongly knit,\nTo thee I send this written embassage,\nTo witness duty, not to show my wit:\nDuty so great, which wit so poor as mine\nMay make seem bare, in wanting words to show it,\nBut that I hope some good conceit of thine\nIn thy soul's thought, all naked, will bestow it:\nTill whatsoever star that guides my moving,\nPoints on me graciously with fair aspect,\nAnd puts apparel on my tatter'd loving,\nTo show me worthy of thy sweet respect:\nThen may I dare to boast how I do love thee;\nTill then, not show my head where thou mayst prove me."
},
{
   "title":"Sonnet 27",
   "content": "Weary with toil, I haste me to my bed,\nThe dear respose for limbs with travel tir'd;\nBut then begins a journey in my head\nTo work my mind, when body's work's expired:\nFor then my thoughts—from far where I abide—\nIntend a zealous pilgrimage to thee,\nAnd keep my drooping eyelids open wide,\nLooking on darkness which the blind do see:\nSave that my soul's imaginary sight\nPresents thy shadow to my sightless view,\nWhich, like a jewel (hung in ghastly night,\nMakes black night beauteous, and her old face new.\nLo! thus, by day my limbs, by night my mind,\nFor thee, and for myself, no quiet find."
},
{
   "title":"Sonnet 28",
   "content": "How can I then return in happy plight,\nThat am debarre'd the benefit of rest?\nWhen day's oppression is not eas'd by night,\nBut day by night and night by day oppress'd,\nAnd each, though enemies to either's reign,\nDo in consent shake hands to torture me,\nThe one by toil, the other to complain\nHow far I toil, still farther off from thee.\nI tell the day, to please him thou art bright,\nAnd dost him grace when clouds do blot the heaven:\nSo flatter I the swart-complexion'd night,\nWhen sparkling stars twire not thou gild'st the even.\nBut day doth daily draw my sorrows longer,\nAnd night doth nightly make grief's length seem stronger."
},
{
   "title":"Sonnet 29",
   "content": "When in disgrace with fortune and men's eyes\nI all alone beweep my outcast state,\nAnd trouble deaf heaven with my bootless cries,\nAnd look upon myself, and curse my fate,\nWishing me like to one more rich in hope,\nFeatur'd like him, like him with friends possess'd,\nDesiring this man's art, and that man's scope,\nWith what I most enjoy contented least;\nYet in these thoughts my self almost despising,\nHaply I think on thee,— and then my state,\nLike to the lark at break of day arising\nFrom sullen earth, sings hymns at heaven's gate;\nFor thy sweet love remember'd such wealth brings\nThat then I scorn to change my state with kings."
},
{
   "title":"Sonnet 30",
   "content": "When to the sessions of sweet silent thought\nI summon up remembrance of things past,\nI sigh the lack of many a thing I sought,\nAnd with old woes new wail my dear time's waste:\nThen can I drown an eye, unused to flow,\nFor precious friends hid in death's dateless night,\nAnd weep afresh love's long since cancell'd woe,\nAnd moan the expense of many a vanish'd sight:\nThen can I grieve at grievances foregone,\nAnd heavily from woe to woe tell o'er\nThe sad account of fore-bemoaned moan,\nWhich I new pay as if not paid before.\nBut if the while I think on thee, dear friend,\nAll losses are restor'd and sorrows end."
},
{
   "title":"Sonnet 31",
   "content": "Thy bosom is endeared with all hearts,\nWhich I by lacking have supposed dead;\nAnd there reigns Love, and all Love's loving parts,\nAnd all those friends which I thought buried.\nHow many a holy and obsequious tear\nHath dear religious love stol'n from mine eye,\nAs interest of the dead, which now appear\nBut things remov'd that hidden in thee lie!\nThou art the grave where buried love doth live,\nHung with the trophies of my lovers gone,\nWho all their parts of me to thee did give,\nThat due of many now is thine alone:\nTheir images I lov'd, I view in thee,\nAnd thou—all they—hast all the all of me."
},
{
   "title":"Sonnet 32",
   "content": "If thou survive my well-contented day,\nWhen that churl Death my bones with dust shall cover\nAnd shalt by fortune once more re-survey\nThese poor rude lines of thy deceased lover,\nCompare them with the bett'ring of the time,\nAnd though they be outstripp'd by every pen,\nReserve them for my love, not for their rhyme,\nExceeded by the height of happier men.\nO! then vouchsafe me but this loving thought:\n'Had my friend's Muse grown with this growing age,\nA dearer birth than this his love had brought,\nTo march in ranks of better equipage:\nBut since he died and poets better prove,\nTheirs for their style I'll read, his for his love'."
},
{
   "title":"Sonnet 33",
   "content": "Full many a glorious morning have I seen\nFlatter the mountain tops with sovereign eye,\nKissing with golden face the meadows green,\nGilding pale streams with heavenly alchemy;\nAnon permit the basest clouds to ride\nWith ugly rack on his celestial face,\nAnd from the forlorn world his visage hide,\nStealing unseen to west with this disgrace:\nEven so my sun one early morn did shine,\nWith all triumphant splendour on my brow;\nBut out! alack! he was but one hour mine,\nThe region cloud hath mask'd him from me now.\nYet him for this my love no whit disdaineth;\nSuns of the world may stain when heaven's sun staineth."
},
{
   "title":"Sonnet 34",
   "content": "Why didst thou promise such a beauteous day,\nAnd make me travel forth without my cloak,\nTo let base clouds o'ertake me in my way,\nHiding thy bravery in their rotten smoke?\n'Tis not enough that through the cloud thou break,\nTo dry the rain on my storm-beaten face,\nFor no man well of such a salve can speak,\nThat heals the wound, and cures not the disgrace:\nNor can thy shame give physic to my grief;\nThough thou repent, yet I have still the loss:\nThe offender's sorrow lends but weak relief\nTo him that bears the strong offence's cross.\nAh! but those tears are pearl which thy love sheds,\nAnd they are rich and ransom all ill deeds."
},
{
   "title":"Sonnet 35",
   "content": "No more be griev'd at that which thou hast done:\nRoses have thorns, and silver fountains mud:\nClouds and eclipses stain both moon and sun,\nAnd loathsome canker lives in sweetest bud.\nAll men make faults, and even I in this,\nAuthorizing thy trespass with compare,\nMyself corrupting, salving thy amiss,\nExcusing thy sins more than thy sins are;\nFor to thy sensual fault I bring in sense,—\nThy adverse party is thy advocate,—\nAnd 'gainst myself a lawful plea commence:\nSuch civil war is in my love and hate,\nThat I an accessary needs must be,\nTo that sweet thief which sourly robs from me."
},
{
   "title":"Sonnet 36",
   "content": "Let me confess that we two must be twain,\nAlthough our undivided loves are one:\nSo shall those blots that do with me remain,\nWithout thy help, by me be borne alone.\nIn our two loves there is but one respect,\nThough in our lives a separable spite,\nWhich though it alter not love's sole effect,\nYet doth it steal sweet hours from love's delight.\nI may not evermore acknowledge thee,\nLest my bewailed guilt should do thee shame,\nNor thou with public kindness honour me,\nUnless thou take that honour from thy name:\nBut do not so, I love thee in such sort,\nAs thou being mine, mine is thy good report."
}
]