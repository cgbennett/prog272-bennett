var MongoClient = require('mongodb').MongoClient;
var format = require('util').format;
var fs = require('fs');


var url1 = "mongodb://10.0.1.9:27017/poemsdb";
var url2 = 'mongodb://chris:hola@ds045907.mongolab.com:45907/finaldb';
var mongodb_url = url2;

MongoClient.connect(mongodb_url, function(err, db) {
	"use strict";
	if (err)
		throw err;

	var collection = db.collection('markdown');
	var poems = fs.readFileSync(__dirname + '/shakespeare.json', 'utf8');
	console.log(poems);
	
	poems = JSON.parse(poems);
	console.log("First poem:" + JSON.stringify(poems[0]));
	
	// js-hint complained about creating a function within a loop.
	// So I'm creating is seperately here.
	// This function is the callback for the mongo insert.
	var insert_callback = function(err, docs) {
		console.log("Insert complete for item #"+JSON.stringify(docs));
	};
	
	for (var i = 0; i < poems.length && i <=35 ; i++) {
		var keywords = ['poem', 'sonnet', (i+1).toString()];
		
		// If content has the word "love" in it,
		// then add keyword "love".
		var n = poems[i].content.indexOf("love");
		if( n >= 0 )
			keywords.push("love");
		
		collection.insert({
			"ItemName" : poems[i].title.replace(/ /g, ''),
			"FolderName" : "Sonnets01",
			"FileName": (poems[i].title + ".md").replace(/ /g, ''),
			"keywords": keywords,
			"Content": poems[i].content
		}, insert_callback);
	}

});
