define([ 'jquery' ], function() {
	'use strict';

	var buttons = null;
	var options = null;
	var transformOptions = null;
	var dataIndex = 0;
	var dataIndexTransform = 0;

	// global variables to save the currently selected options and config.
	var currentOptions = null;
	var currentConfig = null;

	function AwsUi() {
		$("#listBuckets").click(listBuckets);
		$("#copyToS3").click(copyToS3);
		$("#getOptions").click(getOptions);
		$("#transformForwardButton").click(forwardTransform);
		$("#tranformBackButton").click(backwardTransform);
		$("#forwardButton").click(forward);
		$("#backButton").click(backward);

		$("#buildAll").click(buildAll);

		$("#btnSaveConfig").click(saveConfig);
		$("#btnSaveOptions").click(saveOptions);


		getBuildConfig();
		getOptions();
	}

	var buildAll = function() {
		var option = {
				_id : options[dataIndex]._id,
				pathToConfig : $("#pathToConfig").val(),
				reallyWrite : $("#reallyWrite").val(),
				bucketName : $("#bucketName").val(),
				folderToWalk : $("#folderToWalk").val(),
				s3RootFolder : $("#s3RootFolder").val(),
				createFolderToWalkOnS3 : $("#createFolderToWalkOnS3").val(),
				createIndex : $("#createIndex").val(),
				filesToIgnore : $("#filesToIgnore").val()
			};
		
		var config = {
				_id : transformOptions[dataIndexTransform]._id,
				pathToPython : $("#pathToPython").val(),
				copyFrom : $("#copyFrom").val(),
				copyTo : $("#copyTo").val(),
				filesToCopy : $("#filesToCopy").val()
			};
		
		
		$.getJSON("/buildAll", {
			options : option,
			config : config
		}, function(result) {
			$("#buildData").empty();
			var fileArray = result.data.split("\n");
			for ( var i = 0; i < fileArray.length; i++) {
				if (fileArray[i].length > 0) {
					$("#buildData").append("<li>" + fileArray[i] + "</li>");
				}
			}
		});
	};

	var copyToS3 = function() {
		var option = {
				_id : options[dataIndex]._id,
				pathToConfig : $("#pathToConfig").val(),
				reallyWrite : $("#reallyWrite").val(),
				bucketName : $("#bucketName").val(),
				folderToWalk : $("#folderToWalk").val(),
				s3RootFolder : $("#s3RootFolder").val(),
				createFolderToWalkOnS3 : $("#createFolderToWalkOnS3").val(),
				createIndex : $("#createIndex").val(),
				filesToIgnore : $("#filesToIgnore").val()
			};
		
		$.getJSON("/copyToS3", {
			options : JSON.stringify(option)
		}, function(data) {
			$("#copyResult").html("Result: " + data.result);
		});
	};

	var saveOptions = function() {
		var option = {
			_id : options[dataIndex]._id,
			pathToConfig : $("#pathToConfig").val(),
			reallyWrite : $("#reallyWrite").val(),
			bucketName : $("#bucketName").val(),
			folderToWalk : $("#folderToWalk").val(),
			s3RootFolder : $("#s3RootFolder").val(),
			createFolderToWalkOnS3 : $("#createFolderToWalkOnS3").val(),
			createIndex : $("#createIndex").val(),
			filesToIgnore : $("#filesToIgnore").val()
		};
		$.getJSON("/saveOptions", option, function(data) {
			$("#copyResult").html("Result: " + data.result);
		});
	};

	var saveConfig = function() {
		var config = {
			_id : transformOptions[dataIndexTransform]._id,
			pathToPython : $("#pathToPython").val(),
			copyFrom : $("#copyFrom").val(),
			copyTo : $("#copyTo").val(),
			filesToCopy : $("#filesToCopy").val()
		};

		$.getJSON("/saveConfig", config, function(data) {
			$("#copyResult").html("Result: " + data.result);
		});
	};

	var displayTransformConfig = function(options) {
		$("#pathToPython").val(options.pathToPython);
		$("#copyFrom").val(options.copyFrom);
		$("#copyTo").val(options.copyTo);
		$("#filesToCopy").val(options.filesToCopy);
	};

	var displayOptions = function(options) {
		$("#currentDocument").html(dataIndex + 1);
		$("#pathToConfig").val(options.pathToConfig);
		$("#reallyWrite").val(options.reallyWrite ? "true" : "false");
		$("#bucketName").val(options.bucketName);
		$("#folderToWalk").val(options.folderToWalk);
		$("#s3RootFolder").val(options.s3RootFolder);
		$("#createFolderToWalkOnS3").val(
				options.createFolderToWalkOnS3 ? "true" : "false");
		$("#createIndex").val(options.createIndex ? "true" : "false");
		$("#filesToIgnore").val(options.filesToIgnore);
	};

	var getBuildConfig = function() {
		$.getJSON("/getBuildConfig", function(optionsInit) {
			transformOptions = optionsInit;
			displayTransformConfig(transformOptions[dataIndexTransform]);
		}).fail(function(a) {
			alert(JSON.stringify(a));
		});
	};
	var getOptions = function() {
		$.getJSON("/getOptions", function(optionsInit) {
			options = optionsInit;
			$('#documentCount').html(options.length);
			displayOptions(options[0]);
		}).fail(function(a) {
			alert(JSON.stringify(a));
		});
	};

	var forwardTransform = function() {
		if (dataIndexTransform < transformOptions.length - 1) {
			dataIndexTransform++;
			currentConfig = transformOptions[dataIndexTransform];// Save
			// newly
			// selected
			// config
			displayTransformConfig(transformOptions[dataIndexTransform]);
		}
	};

	var backwardTransform = function() {
		if (dataIndexTransform > 0) {
			dataIndexTransform--;
			currentConfig = transformOptions[dataIndexTransform]; // Save
			// newly
			// selected
			// config
			displayTransformConfig(transformOptions[dataIndexTransform]);
			return dataIndexTransform;
		}
		return dataIndexTransform;
	};

	var forward = function() {
		if (dataIndex < options.length - 1) {
			dataIndex++;
			currentOptions = options[dataIndex]; // Save newly selected
			// option
			displayOptions(options[dataIndex]);
		}
	};

	var backward = function() {
		if (dataIndex > 0) {
			dataIndex--;
			currentOptions = options[dataIndex]; // Save newly selected
			// option

			displayOptions(options[dataIndex]);
			return true;
		}
		return false;
	};

	var listBuckets = function() {
		$.getJSON("/listBuckets", {
			options : JSON.stringify(options[dataIndex])
		}, function(data) {
			for ( var i = 0; i < data.length; i++) {
				$("#buckets").append("<li>" + data[i] + "</li>");
			}
		});
	};

	return AwsUi;
});
