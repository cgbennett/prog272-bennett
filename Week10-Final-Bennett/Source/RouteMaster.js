
define([ 'jquery', 'pagedownsetup' ], function(jquery, pagedownsetup) {
	"use strict";
	var allPoems = null;
	var currentPoemId = null;
	var selected_id = null;
	
	var $selectedMarkdown = null;
	
	var that = null;
	
	var server_url = "";
	//var server_url = "http://192.168.1.78:30025";

	// Constructor
	function RouteMaster() {
		$("#btnAllPoems").click(getAllMarkdown);
		$('#btnDelete').on("click", deletePoem);
		$('#btnSearch').on("click", searchPoems);
		$('#btnAdd').on("click", addPoem);
		$('#btnSave').on("click", savePoem);
		$('#btnUpdate').on("click", updatePoem);
		
		that = this;
	}


	var getAllMarkdown = function(event) {

		$('#allMarkdown').html(""); // Clear out poems list section.

		if ( typeof event === "function" ) {
			alert("/getAllMarkdown - shouldnt have gotten here.");
			$.get(server_url+'/getAllMarkdown', event);
		} else {
			$.get(server_url+'/getAllMarkdown', function(data) {
				console.log(JSON.stringify(data));
				allPoems = data;
				for ( var i = 0; i < data.length; i++) {
					var $mydiv = $("<div/>");
					$mydiv.data('poem', data[i]);
					$mydiv.html(data[i].ItemName);

					$('#allMarkdown').append($mydiv);
					//var html = $('#allMarkdown').html();
				}
				$('#allMarkdown div').click(selectPoem);

			});
		}
	};

	var selectPoem = function() {

		var poem = $(this).data('poem');

		selected_id = poem._id;

		// Unselect background color of previous selection
		$('#allMarkdown div').css("background-color", "#007777");

		// Re-create hover css for selector DIV
		$("#allMarkdown div").hover(function() {
			var hovered_poem = $(this).data('poem');
			if (selected_id === hovered_poem._id)
				$(this).css("background-color", "red");
			else
				$(this).css("background-color", "blue");
		}, function() {
			var hovered_poem = $(this).data('poem');
			if (selected_id === hovered_poem._id)
				$(this).css("background-color", "red");
			else
				$(this).css("background-color", "#007777");
		});

		// Set background color of selected poem
		$(this).css("background-color", "red");

		$('#txtItemName').val(poem.ItemName);
		$('#txtFolderName').val(poem.FolderName);
		$('#txtFileName').val(poem.FileName);
		$('#wmd-input').val(poem.Content);
		
		// Keywords
		var keywords = "";
		for ( var i = 0; i < poem.keywords.length; i++) {
			keywords += poem.keywords[i];
			if (i < poem.keywords.length - 1)
				keywords += ", ";
		}
		$('#txtKeywords').val(keywords);
		currentPoemId = poem._id;
		
		// Make sure preview matches the changes.
		pagedownsetup.refreshPreview();
		
		$selectedMarkdown = $(this);

		/*
		// Make sure buttons are displayed correctly.
		$("#btnAdd").css("display", "inline");
		$("#btnDelete").css("display", "inline");
		$("#btnSave").css("display", "none");

		$("#divButtons").css("text-align", "center");
		*/
	};

	var searchPoems = function() {

		$('#allMarkdown').html(""); // Clear out poems section.

		var keyword = $('#txtSearch').val();
		$.ajax({
			url : server_url+"/searchMarkdown",
			type : "GET",
			data : {
				"keyword" : keyword
			},
			dataType : "json",
			success : function(data) {
				console.log(JSON.stringify(data));
				allPoems = data;
				for ( var i = 0; i < data.length; i++) {
					var $mydiv = $("<div/>");
					$mydiv.data('poem', data[i]);
					$mydiv.html(data[i].ItemName);

					$('#allMarkdown').append($mydiv);
					//var html = $('#allMarkdown').html();

				}

				$('#allMarkdown div').click(selectPoem);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR.responseText);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	};

	var addPoem = function() {

		$('#txtTitle').val("");
		$('#wmd-input').val("");
		$('#txtAuthor').val("");
		$('#txtKeywords').val("");

		/*
		$("#btnAdd").css("display", "none");
		$("#btnDelete").css("display", "none");
		$("#btnSave").css("display", "inline");
		$("#divButtons").css("text-align", "center");
*/
	};

	 var savePoem = function() {

		var title = $('#txtTitle').val();
		var content = $('#wmd-input').val();
		var author = $('#txtAuthor').val();
		var keywords = $('#txtKeywords').val();

		$.ajax({
			url : server_url+"/savePoem",
			type : "GET",
			data : {
				"title" : title,
				"content" : content,
				"author" : author,
				"keywords" : keywords
			},
			dataType : "json",
			success : function(data) {
				// show delete and add buttons again. Hide save.
				/*
				$("#btnAdd").css("display", "inline");
				$("#btnDelete").css("display", "inline");
				$("#btnSave").css("display", "none");

				$("#divButtons").css("text-align", "center");

				that.getAllPoems();
				*/
			},
			error : function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR.responseText);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	};
	
	
	 var updatePoem = function() {
			var itemname = $('#txtItemName').val();
			var foldername = $('#txtFolderName').val();
			var filename = $('#txtFileName').val();
			var keywords = $('#txtKeywords').val();
			var content = $('#wmd-input').val();

			$.ajax({
				url : server_url+"/updateMarkdown",
				type : "GET",
				data : {
					"_id": selected_id,
					"ItemName" : itemname,
					"FolderName" : foldername,
					"FileName" : filename,
					"keywords" : keywords,
					"Content" : content
				},
				dataType : "json",
				success : function(data) {
					
					var itemdata = $selectedMarkdown.data().poem;
					
					itemdata.ItemName = itemname;
					itemdata.FolderName = foldername;
					itemdata.FileName = filename;
					itemdata.Content = content;
					$selectedMarkdown.data(itemdata);
					
					// show delete and add buttons again. Hide save.
					//$("#btnAdd").css("display", "inline");
					//$("#btnDelete").css("display", "inline");
					//$("#btnSave").css("display", "none");

					//$("#divButtons").css("text-align", "center");

					//getAllMarkdown();
				},
				error : function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR.responseText);
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
		};
	
	
	

	var deletePoem = function() {

		if (!currentPoemId) {
			alert("No poem selected.");
			return;
		}

		var isSure = confirm("Are you sure you want to delete this poem?");
		if (!isSure) {
			return;
		}

		$.ajax({
			url : server_url+"/deletePoem",
			type : "GET",
			data : {
				"poemId" : currentPoemId
			},
			dataType : "json",
			success : function(data) {
				console.log(JSON.stringify(data));
				that.getAllPoems();

				// Clear text displays
				$('#txtTitle').val("");
				$('#wmd-input').val("");
				$('#txtAuthor').val("");
				$('#txtKeywords').val("");

				console.log("Delete sucessful");
			},
			error : function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR.responseText);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	};

	// Return constructor
	return RouteMaster;
});
