/**
 * @author Charlie Calvert
 */

require.config({
  paths: {
    "jquery": "http://code.jquery.com/jquery-1.11.0.min",
    "awsui": "AwsUi"  ,
    "routemaster": "RouteMaster",
    "pagedownsetup": "PagedownSetup"
  }
});

require(["awsui", "routemaster", "pagedownsetup"], function(awsui, routemaster, pagedownsetup) { 'use strict';
	console.log("Main called.");
	awsui();
	routemaster();
	pagedownsetup();
});
