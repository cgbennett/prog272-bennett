var MongoData = (function() {'use strict';

	var mongoData = null;

	function MongoData() {
		$("#readAll").click(queryAll);
		$("#readTwo").click(queryTwo);
		$("#newRecord").click(createNewRecord);
		$("#showData").click(showData);
	}

	var displayRecord = function(index) {
		$('#firstName').html(mongoData[index].firstName);
		$('#lastName').html(mongoData[index].lastName);
		$('#address').html(mongoData[index].address);
		$('#city').html(mongoData[index].city);
		$('#state').html(mongoData[index].state);
		$('#zip').html(mongoData[index].zip);
	};

	var getNewRecord = function() {
		return {
			firstName : "Chris",
			lastName : "Bennett",
			address : "123 Oak Street",
			city : "Seattle",
			state : "Washington",
			zip : "98444"
		};
	};

	var showData = function() {
		var index = $("#userIndex").val();
		displayRecord(index);
	};

	var queryAll = function() {
		$.getJSON('/readAll', function(data) {
			mongoData = data;
			console.log(data);
			displayRecord(0);
			$("#mongoData").empty();
			for (var i = 0; i < data.length; i++) {
				$("#mongoData").append('<li>' + JSON.stringify(data[i]) + '</li>');
			}
		});
	};

	var createNewRecord = function() {
		var insertRecord = getNewRecord();
		$.ajax({
			dataType : "json",
			data : insertRecord,
			url : '/addRecord',
			success : function(data) {
				mongoData = data;
				console.log(data);
			},
			error: function(err){
				alert("error!");
			}
		});
	};

	var queryTwo = function() {
		$.getJSON('/readTwo', function(data) {
			mongoData = data;
			console.log(data);
			displayRecord(0);
			$("#mongoData").empty();
			for (var i = 0; i < data.length; i++) {
				$("#mongoData").append('<li>' + JSON.stringify(data[i]) + '</li>');
			}
		});
	};

	return MongoData;
})();

$(document).ready(function() {'use strict';
	var o = new MongoData();

});
