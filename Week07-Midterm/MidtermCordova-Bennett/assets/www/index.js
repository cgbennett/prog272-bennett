var RouteMaster = (function() {
	"use strict";
	var allPoems = null;
	var currentPoemId = null;
	var selected_id = null;
	
	var that = null;
	
	//var server_url = "";
	var server_url = "http://192.168.1.78:30025";

	// Constructor
	function RouteMaster() {
		$("#btnAllPoems").click(this.getAllPoems);
		$('#btnDelete').on("click", this.deletePoem);
		$('#btnSearch').on("click", this.searchPoems);
		$('#btnAdd').on("click", this.addPoem);
		$('#btnSave').on("click", this.savePoem);
		
		that = this;
	}
	
	RouteMaster.prototype.getServerUrl = function()
	{
		return server_url;
	};

	RouteMaster.prototype.getAllPoems = function(event) {

		$('#allPoems').html(""); // Clear out poems list section.

		if ( typeof event === "function" ) {
			$.get(server_url+'/getAllPoems', event);
		} else {
			$.get(server_url+'/getAllPoems', function(data) {
				console.log(JSON.stringify(data));
				allPoems = data;
				for ( var i = 0; i < data.length; i++) {
					var $mydiv = $("<div/>");
					$mydiv.data('poem', data[i]);
					$mydiv.html(data[i].title);

					$('#allPoems').append($mydiv);
					var html = $('#allPoems').html();
				}
				$('#allPoems div').click(that.selectPoem);

			});
		}
	};

	RouteMaster.prototype.selectPoem = function() {

		var poem = $(this).data('poem');

		selected_id = poem._id;

		// Unselect background color of previous selection
		$('#allPoems div').css("background-color", "#007777");

		// Re-create hover css for selector DIV
		$("#allPoems div").hover(function() {
			var hovered_poem = $(this).data('poem');
			if (selected_id === hovered_poem._id)
				$(this).css("background-color", "red");
			else
				$(this).css("background-color", "blue");
		}, function() {
			var hovered_poem = $(this).data('poem');
			if (selected_id === hovered_poem._id)
				$(this).css("background-color", "red");
			else
				$(this).css("background-color", "#007777");
		});

		// Set background color of selected poem
		$(this).css("background-color", "red");

		$('#txtTitle').val(poem.title);
		$('#txtContent').val(poem.content);
		$('#txtAuthor').val(poem.author);

		// Keywords
		var keywords = "";
		for ( var i = 0; i < poem.keywords.length; i++) {
			keywords += poem.keywords[i];
			if (i < poem.keywords.length - 1)
				keywords += ", ";
		}
		$('#txtKeywords').val(keywords);
		currentPoemId = poem._id;

		// Make sure buttons are displayed correctly.
		$("#btnAdd").css("display", "inline");
		$("#btnDelete").css("display", "inline");
		$("#btnSave").css("display", "none");

		$("#divButtons").css("text-align", "center");
	};

	RouteMaster.prototype.searchPoems = function() {

		$('#allPoems').html(""); // Clear out poems section.

		var keyword = $('#txtSearch').val();
		$.ajax({
			url : server_url+"/searchPoems",
			type : "GET",
			data : {
				"keyword" : keyword
			},
			dataType : "json",
			success : function(data) {
				console.log(JSON.stringify(data));
				allPoems = data;
				for ( var i = 0; i < data.length; i++) {
					var $mydiv = $("<div/>");
					$mydiv.data('poem', data[i]);
					$mydiv.html(data[i].title);

					$('#allPoems').append($mydiv);
					var html = $('#allPoems').html();

				}

				$('#allPoems div').click(that.selectPoem);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR.responseText);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	};

	RouteMaster.prototype.addPoem = function() {

		$('#txtTitle').val("");
		$('#txtContent').val("");
		$('#txtAuthor').val("");
		$('#txtKeywords').val("");

		$("#btnAdd").css("display", "none");
		$("#btnDelete").css("display", "none");
		$("#btnSave").css("display", "inline");
		$("#divButtons").css("text-align", "center");

	};

	RouteMaster.prototype.savePoem = function() {

		var title = $('#txtTitle').val();
		var content = $('#txtContent').val();
		var author = $('#txtAuthor').val();
		var keywords = $('#txtKeywords').val();

		$.ajax({
			url : server_url+"/savePoem",
			type : "GET",
			data : {
				"title" : title,
				"content" : content,
				"author" : author,
				"keywords" : keywords
			},
			dataType : "json",
			success : function(data) {
				// show delete and add buttons again. Hide save.
				$("#btnAdd").css("display", "inline");
				$("#btnDelete").css("display", "inline");
				$("#btnSave").css("display", "none");

				$("#divButtons").css("text-align", "center");

				that.getAllPoems();
			},
			error : function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR.responseText);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	};

	RouteMaster.prototype.deletePoem = function() {

		if (!currentPoemId) {
			alert("No poem selected.");
			return;
		}

		var isSure = confirm("Are you sure you want to delete this poem?");
		if (!isSure) {
			return;
		}

		$.ajax({
			url : server_url+"/deletePoem",
			type : "GET",
			data : {
				"poemId" : currentPoemId
			},
			dataType : "json",
			success : function(data) {
				console.log(JSON.stringify(data));
				that.getAllPoems();

				// Clear text displays
				$('#txtTitle').val("");
				$('#txtContent').val("");
				$('#txtAuthor').val("");
				$('#txtKeywords').val("");

				console.log("Delete sucessful");
			},
			error : function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR.responseText);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	};

	// Return constructor
	return RouteMaster;
}());

$(document).ready(function() {
	"use strict";
	new RouteMaster();
});