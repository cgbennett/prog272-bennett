describe("Client Side Suite", function() {
	'use strict';
	var routeMaster = null;

	beforeEach(function() {
		routeMaster = new RouteMaster();
	});

	it("Proves Jasmine is working", function() {
		expect(true).toBe(true);
	});

	it("Proves we can create RouteMaster", function() {
		expect(routeMaster).not.toBeNull();
	});

	it("Performs Async intergration test on getAllPoems", function(done) {
		routeMaster.getAllPoems(function(data) {
			expect(data.length).toBeGreaterThan(0);
			done();
		});
	});

	it("Performs a mock test - a spy - on jquery.get ", function() {
		spyOn($, "get");
		routeMaster.getAllPoems(null);
		expect($.get).toHaveBeenCalled();

	});

	it("Performs a mock test - a spy - on jquery.get parameters", function() {
		spyOn($, "get");
		
		var callback = function(data){};
		routeMaster.getAllPoems(callback);
		var server_url = routeMaster.getServerUrl();
		expect($.get).toHaveBeenCalledWith(server_url+"/getAllPoems", callback);
	});

});
