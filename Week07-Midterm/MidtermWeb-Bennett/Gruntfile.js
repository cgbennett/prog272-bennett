module.exports = function(grunt) {
	'use strict';

	grunt.initConfig({

		jshint : {
			files : [ '**/*.js' ],

			options : {
				ignores : [ '**/coverage/**', '**/node_modules/**',
						'**/angular.js', '**/angular-mocks.js',
						'**/jquery*.js', '**/bootstrap.min.js', '**/crafty.js',
						'**/qunit-1.13.0.css', '**/qunit-1.13.0.js',
						'**/boot.js', '**/console.js', '**/jasmine.js',
						'**/jasmine-html.js', '**/jasmine-jquery.js' ],
				reporter : 'checkstyle',
				reporterOutput : 'result.xml',
				strict : true,
				newcap : false,
				globals : {
					describe : true,
					afterEach : true,
					beforeEach : true,
					inject : true,
					it : true,
					jasmine : true,
					expect : true,
					angular : true,
					module : true,
					Crafty : true,
					require : true,
					console : true,
					__dirname : true,
					process : true
				}
			}
		},
		jasmine_node : {
			projectRoot : ".",
			requirejs : false,
			forceExit : true,
			extensions : 'js',
			specNameMatcher : 'spec'
		}

	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-jasmine-node');

	grunt.registerTask('default', [ 'jshint', 'jasmine_node' ]);

};
