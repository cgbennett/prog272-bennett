/**
 * @author Chris Bennett
 */

/* jshint -W099 */

var fs = require('fs');
    

var loadServerConfigFile = function()
{
    "use strict";
    
    var filename = __dirname + '/server_config.json';
    
    var data = JSON.parse(fs.readFileSync(filename));

    return data;
};



module.exports = loadServerConfigFile();
