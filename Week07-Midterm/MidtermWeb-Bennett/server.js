

var express = require('express');
var app = express();
var fs = require('fs');
var mongo = require('mongodb');
var serverconfig = require('./serverconfig');
var MongoClient = require('mongodb').MongoClient;
var BSON = mongo.BSONPure;



var url1 = "mongodb://10.0.1.9:27017/poemsdb";
var url2 = 'mongodb://192.168.1.104:27017/poemsdb';
var mongodb_url = serverconfig.Mongo_Server_Url;
//"Mongo_Server_Url": "mongodb://chris:blah1234@ds033599.mongolab.com:33599/poemsdb"

app.use(express.bodyParser());

var port = process.env.PORT || 30025;

app.get('/getAllPoems', function(request, response) {
	"use strict";
	console.log('getAllPoems called');
	MongoClient.connect(mongodb_url, function(err, database) {
		if (err) {
			throw err;
		}
		var collection = database.collection('poems');
		collection.find().sort({
			"title" : 1
		}).toArray(function(err, theArray) {
			// console.dir(theArray);
			database.close();
			response.send(theArray);
		});
	});
});

app.get('/searchPoems', function(request, response) {
	"use strict";
	console.log('searchPoems called');
	MongoClient.connect(mongodb_url, function(err, database) {
		if (err) {
			throw err;
		}
		var keyword = request.query.keyword;
		console.log("params = " + request.query);
		console.log("keyword =" + keyword);
		var collection = database.collection('poems');
		collection.find({
			keywords : keyword
		}).sort({
			"title" : 1
		}).toArray(function(err, theArray) {
			// console.dir(theArray);
			database.close();
			response.send(theArray);
		});
	});
});

app.get('/deletePoem', function(request, response) {
	"use strict";
	console.log('deletePoem called');
	var poemId = request.query.poemId;
	console.log("poemId = " + poemId);
	MongoClient.connect(mongodb_url, function(err, database) {
		if (err) {
			throw err;
		}
		var o_id = new BSON.ObjectID(poemId);
		console.log("o_id = " + o_id);

		var collection = database.collection('poems');
		collection.remove({
			"_id" : o_id
		}, function(err, removed) {
			if (err)
				console.log(err);
			console.log(removed);
			database.close();
			response.send({
				message : "Success"
			});
		});
	});
});

app.get('/savePoem', function(request, response) {
	"use strict";
	console.log('savePoem called');
	console.log(JSON.stringify(request.query));

	// Make keywords into an array.
	var keywords = request.query.keywords;
	request.query.keywords = keywords.split(',');
	for ( var i = 0; i < request.query.keywords.length; i++) {
		request.query.keywords[i] = request.query.keywords[i].trim();
	}
	console.log(JSON.stringify(request.query));

	MongoClient.connect(mongodb_url, function(err, database) {
		if (err) {
			throw err;
		}
		var collection = database.collection('poems');
		collection.insert(request.query, function(err, removed) {
			if (err)
				console.log(err);
			console.log(removed);
			database.close();
			response.send({
				message : "Success"
			});
		});
	});
});

app.get('/', function(request, response) {
	"use strict";
	var html = fs.readFileSync(__dirname + '/Public/index.html');
	response.writeHeader(200, {
		"Content-Type" : "text/html"
	});
	response.write(html);
	response.end();
});

app.use("/", express.static(__dirname + '/Public'));
app.listen(port);
console.log('Listening on port :' + port);
