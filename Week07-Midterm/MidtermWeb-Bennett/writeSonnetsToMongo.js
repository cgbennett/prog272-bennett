var MongoClient = require('mongodb').MongoClient;
var format = require('util').format;
var fs = require('fs');
var serverconfig = require('./serverconfig');


var mongodb_url = serverconfig.Mongo_Server_Url;

MongoClient.connect(mongodb_url, function(err, db) {
	"use strict";
	if (err)
		throw err;

	var collection = db.collection('poems');
	var poems = fs.readFileSync(__dirname + '/shakespeare.json', 'utf8');
	console.log(poems);
	
	poems = JSON.parse(poems);
	console.log("First poem:" + JSON.stringify(poems[0]));
	
	// js-hint complained about creating a function within a loop.
	// So I'm creating is seperately here.
	// This function is the callback for the mongo insert.
	var insert_callback = function(err, docs) {
		console.log("Insert complete for item #"+JSON.stringify(docs));
	};
	
	for (var i = 0; i < poems.length; i++) {
		var keywords = ['poem', 'sonnet', (i+1).toString()];
		
		// If content has the word "love" in it,
		// then add keyword "love".
		var n = poems[i].content.indexOf("love");
		if( n >= 0 )
			keywords.push("love");
		
		collection.insert({
			"title" : poems[i].title,
			"content" : poems[i].content,
			"author": "Shakespeare",
			"keywords": keywords
		}, insert_callback);
	}

});
