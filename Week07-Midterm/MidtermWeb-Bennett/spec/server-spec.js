/**
 * @author Charlie Calvert
 */

var request = require('request');

describe("A Mongo Suite", function() { 'use strict';
	var server = 'http://localhost:30025/';
	
	beforeEach(function() {

	});

	it("should just work", function() {
		console.log("Server Jasmine Properly Setup.");
		expect(true).toEqual(true, "Is testing setup properly?");

	});

	it("should get all poems from MongoDB", function(done) {
		request("http://localhost:30025/getAllPoems", function(error, response, output) {
			console.log("getAllPoems called: " + output);
			output = JSON.parse(output);
			//expect(output.length).toBe('Success');
			expect(output.length).toBeGreaterThan(0);
			done();
		});
	});

	
	it("should search poems for keyword 112 from MongoDB", function(done) {
		request("http://localhost:30025/searchPoems?keyword=112", function(error, response, output) {
			console.log("searchPoems called: " + output);
			output = JSON.parse(output);
			expect(output.length).toBe(1);
			expect(output[0].title).toBe('Sonnet 112');
			done();
		});
	});
	

	it("should search poems for keyword 'love' from MongoDB", function(done) {
		request("http://localhost:30025/searchPoems?keyword=love", function(error, response, output) {
			console.log("searchPoems called: " + output);
			output = JSON.parse(output);
			expect(output.length).toBeGreaterThan(0);
			done();
		});
	});

});
