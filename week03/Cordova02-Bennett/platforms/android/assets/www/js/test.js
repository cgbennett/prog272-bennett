describe("Tests for Fahrenheit conversion", function() {
	it("expects true to be true", function() {
		expect(true).toBe(true);
	});

	it("converts from Fahrenheit 100", function() {
		expect(convert.convertToCelsius("100")).toBe("37.78");
	});

	it("converts from Fahrenheit 0", function() {
		expect(convert.convertToCelsius("0")).toBe("-17.78");
	});

	it("converts from Fahrenheit 5", function() {
		expect(convert.convertToCelsius("5")).toBe("-15.00");
	});

	it("converts from Fahrenheit 1000", function() {
		expect(convert.convertToCelsius("1000")).toBe("537.78");
	});

	it("converts from Fahrenheit -20", function() {
		expect(convert.convertToCelsius("-20")).toBe("-28.89");
	});
});

describe("Tests for Miles conversion", function() {
	it("expects true to be true", function() {
		expect(true).toBe(true);
	});

	it("converts from Miles 100", function() {
		expect(convert.convertToKilometers("100")).toBe("160.93");
	});

	it("converts from Miles 0", function() {
		expect(convert.convertToKilometers("0.00")).toBe("0.00");
	});

	it("converts from Miles 5", function() {
		expect(convert.convertToKilometers("5")).toBe("8.05");
	});

	it("converts from Miles -5", function() {
		expect(convert.convertToKilometers("-5")).toBe("-8.05");
	});

	it("converts from Miles 20", function() {
		expect(convert.convertToKilometers("20")).toBe("32.19");
		
	});
});
	
	
	describe("Tests for Square Root calc", function() {
	it("expects true to be true", function() {
		expect(true).toBe(true);
	});

	it("calcs square root 100", function() {
		expect(convert.calcSquareRoot("100")).toBe("10.00");
	});

	it("calcs square root 0", function() {
		expect(convert.calcSquareRoot("0.00")).toBe("0.00");
	});

	it("calcs square root 5", function() {
		expect(convert.calcSquareRoot("5")).toBe("2.24");
	});

	it("calcs square root -5", function() {
		expect(convert.calcSquareRoot("-5")).toBe("NaN");
	});

	it("calcs square root 20", function() {
		expect(convert.calcSquareRoot("20")).toBe("4.47");
		
	});
	
	
	
});