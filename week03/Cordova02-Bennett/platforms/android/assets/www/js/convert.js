var convert = {
	convertToCelsius : function(f) {
		f = parseInt(f);
		//var f = $("#txtFahrenheit").val();
		var c = (f - 32) * 5 / 9;
		return c.toFixed(2);

	},

	convertToKilometers : function(miles) {
		miles = parseInt(miles);
		//var miles = $("#txtMiles").val();
		var km = miles / 0.62137;
		return km.toFixed(2);

	},

	calcSquareRoot : function(n) {
		n = parseInt(n);
		//var n = $("#txtNumber").val();
		var sr = Math.sqrt(n);
		return sr.toFixed(2);
	},

	setupHandlers : function() {

		$("#btnCelsius").click(function() {
			var f = $('#txtFahrenheit').val();
			var c = convert.convertToCelsius(f);
			$("#lblCelsius").html(c);
		});

		$("#btnKilometers").click(function() {
			var f = $('#txtMiles').val();
			var c = convert.convertToKilometers(f);
			$("#lblKilometers").html(c);
		});

		$("#btnSquareRoot").click(function() {
			var f = $('#txtNumber').val();
			var c = convert.calcSquareRoot(f);
			$("#lblSquareRoot").html(c);
		});

	}
};

convert.setupHandlers();
