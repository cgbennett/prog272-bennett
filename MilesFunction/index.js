
/* jshint strict: false */


var milesConvert = 
{
	feetPerMile: 5280,
	milesConvert: function(miles)
	{
		return miles * this.feetPerMile;
	}
};

//var result = milesConvert.milesConvert(3);
//console.log(result);
