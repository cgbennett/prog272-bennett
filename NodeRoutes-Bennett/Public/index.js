var RouteMaster = ( function() {

		// Constructor
		function RouteMaster() {
			$("#btnFeet").click(getFeetInMiles);
			$("#btnConvert").click(convertMilesToFeet);
			$("#btnCircumference").click(calcCircumference);
		}

		var getFeetInMiles = function() {
			$.get('/getFeetInMiles', function(data) {
				console.log(JSON.stringify(data));
				$('#feetResult').html(data.result);
				//nineResult.html("The value sent from the server is: <strong>" + data.result + "</strong>");
			});
		};

		var convertMilesToFeet = function() {
			var miles = $("#txtMiles").val();
			$.ajax({
				url : "/convertMilesToFeet",
				type : "GET",
				data : {
					"miles" : miles
				},
				dataType : "json",
				success : function(data) {	
					console.log(JSON.stringify(data));
					$('#convertResult').html(data.result);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR.responseText);
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
		};

		var calcCircumference = function() {
			var radius = $("#txtRadius").val();

			$.ajax({
				url : "/calculateCircumference",
				type : "POST",
				data : {
					"radius" : radius
				},
				dataType : "json",
				success : function(data) {	
					console.log(JSON.stringify(data));
					$('#calcResult').html(data.result);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR.responseText);
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
		};

		// Return constructor
		return RouteMaster;
	}());

$(document).ready(function() {
	new RouteMaster();
}); 