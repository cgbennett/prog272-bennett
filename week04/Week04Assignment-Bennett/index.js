function MyObject() {
	this.getData = function(i, cb) {

		if (!cb) {
			cb = function(data) {
				var result = "<p>First Name: " + data[i].firstName + "</p>";
				var value = "<p>Last Name: " + data[i].lastName + "</p>";
				$("#div01").html(result + value);
			};
		}

		$.getJSON('index.json', cb)
		.success(function() {
			console.log("csc: success. Loaded index.json");
		}).error(function(jqXHR, textStatus, errorThrown) {
			showError(jqXHR, textStatus, errorThrown);
		}).complete(function() {
			console.log("csc: completed call to get index.json");
		});
	};

	this.getHtml = function(target, source, cb) {
		if(!cb)
		{
			cb = function(responseText, textStatus, XMLHttpRequest) {
				console.log("button pressed");
			};
		}
		
		$('#' + target).load(source, cb );
	};
}


$(document).ready(function() {

	$('#buttonHtml01').on('click', function() {
		var myObject = new MyObject();
		myObject.getHtml("div01", "Sources.html #paragraph01");

	});

	$('#buttonHtml02').on('click', function() {
		var myObject = new MyObject();
		myObject.getHtml("div01", "Sources.html #paragraph02");

	});

	$('#buttonHtml03').on('click', function() {
		var myObject = new MyObject();
		myObject.getHtml("div01", "Sources.html #paragraph03");
	});

	$('#buttonJson01').on('click', function() {
		var myObject = new MyObject();
		myObject.getData(0);
	});

	$('#buttonJson02').on('click', function() {
		var myObject = new MyObject();
		myObject.getData(1);
	});

	$('#buttonJson03').on('click', function() {
		var myObject = new MyObject();
		myObject.getData(2);
	});

	//var myObject = new MyObject();
	//myObject.getData();
	//mmmyObject.getHtml();
});
