describe("JQuery Json Unit Tests", function() {
	it("expects true to be true", function() {
		expect(true).toBe(true);
	});

	it("gets back George for firstName", function(done) {
		var myObject = new MyObject();
		myObject.getData(0, function(data) {
			expect(data[0].firstName).toBe("George");
			done();
		});
	});

	it("gets back Washington for lastName", function(done) {
		var myObject = new MyObject();
		myObject.getData(0, function(data) {
			expect(data[0].lastName).toBe("Washington");
			done();
		});
	});

	it("gets back John for firstName", function(done) {
		var myObject = new MyObject();
		myObject.getData(1, function(data) {
			expect(data[1].firstName).toBe("John");
			done();
		});
	});

	it("gets back Adams for lastName", function(done) {
		var myObject = new MyObject();
		myObject.getData(1, function(data) {
			expect(data[1].lastName).toBe("Adams");
			done();
		});
	});

	it("gets back Thomas for firstName", function(done) {
		var myObject = new MyObject();
		myObject.getData(2, function(data) {
			expect(data[2].firstName).toBe("Thomas");
			done();
		});
	});

	it("gets back Jefferson for lastName", function(done) {
		var myObject = new MyObject();
		myObject.getData(2, function(data) {
			expect(data[2].lastName).toBe("Jefferson");
			done();
		});
	});

});

describe("JQuery Load Unit Tests", function() {
	it("expects true to be true", function() {
		expect(true).toBe(true);
	});

	it("gets #paragraph01 html", function(done) {
		var myObject = new MyObject();
		myObject.getHtml("div01", "Sources.html #paragraph01", function(responseText, textStatus, XMLHttpRequest) {
			var result = $("#div01").html();
			expect(result).toBe('<p id="paragraph01">Paragraph01</p>');
			done();
		});
	});

	it("gets #paragraph02 html", function(done) {
		var myObject = new MyObject();
		myObject.getHtml("div01", "Sources.html #paragraph02", function(responseText, textStatus, XMLHttpRequest) {
			var result = $("#div01").html();
			expect(result).toBe('<p id="paragraph02">Paragraph02</p>');
			done();
		});
	});

	it("gets #paragraph03 html", function(done) {
		var myObject = new MyObject();
		myObject.getHtml("div01", "Sources.html #paragraph03", function(responseText, textStatus, XMLHttpRequest) {
			var result = $("#div01").html();
			expect(result).toBe('<p id="paragraph03">Paragraph03</p>');
			done();
		});
	});

});

