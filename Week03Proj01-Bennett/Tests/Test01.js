describe("Elvenware Jasmine One Suite", function() {
	it("expects true to be true", function() {
		expect(true).toBe(true);
	});

	it("expects to get 9", function() {
		expect(numberGenerator.getNine()).toBe(9);
	});

	it("expects to get 8", function() {
		expect(numberGenerator.getEight()).toBe(8);
	});

});
