/**
 * @author Chris Bennett
 */

var PubSub = {};

define([ 'jquery', 'messenger' ], function() {

		/*
		 * The point is that there is no reference to Publisher in this module
		 * and yet it can recieve messages from it
		 */
		function Subscriber() {
			$.subscribe('add', add);
			$.subscribe('multiply', multiply);
			$.subscribe('subtract', subtract);
		}

		function add(event, input) {
			var sum = input.n1 + input.n2;
			input.callback(sum);
		}

		function multiply(event, input) {
			var product = input.n1 * input.n2;
			input.callback(product);
		}

		function subtract(event, input) {
			var difference = input.n1 - input.n2;
			input.callback(difference);
		}

		return {subscriber: Subscriber};

});
