var express = require('express');
var app = express();
var fs = require('fs');
var getNine = require('./Library/GetNine');
var addingMachine = require('./Library/AddingMachine');
app.use(express.bodyParser());

var port = process.env.PORT || 30025;

app.get('/getFeetInMiles', function(request, response) {
	console.log('getFeetInMiles called');
	response.send({ "result": 5280 });
});

app.get('/convertMilesToFeet', function(request, response) {
	console.log('convertMilesToFeet called');
	
	var miles = parseInt(request.query.miles);
	
	response.send({ "result": miles*5280 });
});

app.put('/calculateCircumference', function(request, response) {
	console.log('calculateCircumference called');
	
	var radius = parseInt(request.body.radius);
	var circumference = 2 * radius * Math.PI;
	
	response.send({ "result": circumference });
});


app.get('/', function(request, response) {
	var html = fs.readFileSync(__dirname + '/Public/index.html');
	response.writeHeader(200, {"Content-Type": "text/html"});   
	response.write(html);
	response.end();
});


app.use("/", express.static(__dirname + '/Public'));
app.listen(port);
console.log('Listening on port :' + port);
