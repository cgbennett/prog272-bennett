/**
 * @author Chris Bennett
 */

PubSub.Publisher = (function() {

	function Publisher() {
		$("#btnAdd").click(add);
		$("#btnMultiply").click(multiply);
		$("#btnSubtract").click(subtract);
	}

	// This function constructs input object passed to Subscriber.
	var createInputObject = function() {
		var input = {
			n1 : parseInt($('#txtNumber1').val()),
			n2 : parseInt($('#txtNumber2').val()),
			callback : function(result) {
				$('#displayResult').html(result);
			}
		};
		return input;
	};

	var add = function() {
		// Consolidate parameters into an object
		var input = createInputObject();
		$.publish('add', input);
	};

	var multiply = function() {
		// Consolidate parameters into an object
		var input = createInputObject();
		$.publish('multiply', input);
	};

	var subtract = function() {
		// Consolidate parameters into an object
		var input = createInputObject();
		$.publish('subtract', input);
	};

	return Publisher;

}());

$(document).ready(function() {
	new PubSub.Subscriber();
	new PubSub.Publisher();
});
