var addressData = null;

function showData() {

	$.getJSON('/read', function(data) {
		console.log(data);
		addressData = data;
		for (var i = 0; i < data.length; i++) {
			$("#mongoData").append('<li>' + JSON.stringify(data[i]) + '</li>');
		}
	});

}


$(document).ready(function() {

	$('#btnShowData').on("click", function() {
		var i = $("#txtIndex").val();
		$('#firstname').text(addressData[i].firstName);
		$('#lastname').text(addressData[i].lastName);
		$('#address').text(addressData[i].address);
		$('#city').html(addressData[i].city);
		$('#state').html(addressData[i].state);
		$('#zip').html(addressData[i].zip);
	});
	showData();
});
