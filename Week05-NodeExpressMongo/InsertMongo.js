var MongoClient = require('mongodb').MongoClient;
var format = require('util').format;

var url01 = 'mongodb://127.0.0.1:27017/test';
var url02 = 'mongodb://192.168.2.19:27017/test';
var url03 = 'mongodb://192.168.1.107:27017/test';

MongoClient.connect(url03, function(err, db) {
	if (err)
		throw err;

	var collection = db.collection('test_insert');
	for (var i = 0; i < 250; i++) {
		collection.insert({
			"firstName" : "Chris" + i,
			"lastName" : "Bennett" + i,
			"address" : " "+ i +" Oak Street",
			"city" : "Seattle",
			"state" : "Washington",
			"zip" : "98444"
		}, function(err, docs) {
			console.log("Insert complete for item #"+JSON.stringify(docs));
		});
	}
});
