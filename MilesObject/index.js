
/* jshint strict: false */


var milesConvert = 
{
	miles: 3,
	feetPerMile: 5280,
	milesToFeet: function()
	{
		return this.miles * this.feetPerMile;
	}
};

