/**
 * @author Chris
 */

describe("Mile Object Project", function() {
	'use strict';
	it("expects true to be true", function() {
		expect(true).toBe(true);
	});

	it("converts 3 miles into feet", function() {
		expect(milesConvert.milesToFeet()).toBe(15840);
	});

	it("converts 0 miles into feet", function() {
		milesConvert.miles = 0;
		expect(milesConvert.milesToFeet()).toBe(0);
	});

	it("converts 10 miles into feet", function() {
		milesConvert.miles = 10;
		expect(milesConvert.milesToFeet()).toBe(52800);
	});

	it("converts -3 miles into feet", function() {
		milesConvert.miles = -3;
		expect(milesConvert.milesToFeet()).toBe(-15840);
	});

});

